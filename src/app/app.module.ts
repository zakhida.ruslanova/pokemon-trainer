import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { PokemonCataloguePage } from './pages/pokemon-catalogue/pokemon-catalogue.page';
import { LandingPage } from './pages/landing/landing.page';
import { NavbarComponent } from './components/navbar/navbar.component';
import { TrainerProfilePage } from './pages/trainer-profile/trainer-profile.page';
import { PokemonListComponent } from './components/pokemon-list/pokemon-list.component';
import { PokemonListItemComponent } from './components/pokemon-list-item/pokemon-list-item.component';
import { CatchButtonComponent } from './components/catch-button/catch-button.component';
import { LandingFormComponent } from './components/landing-form/landing-form.component';

@NgModule({
  declarations: [
    AppComponent,
    LandingPage,
    PokemonCataloguePage,
    NavbarComponent,
    TrainerProfilePage,
    PokemonListComponent,
    PokemonListItemComponent,
    CatchButtonComponent,
    LandingFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }