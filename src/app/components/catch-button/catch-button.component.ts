import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Trainer } from 'src/app/models/trainer.model';
import { CatchService } from 'src/app/services/catch.service';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-catch-button',
  templateUrl: './catch-button.component.html'
})
export class CatchButtonComponent implements OnInit {

  public loading: boolean = false;
  public isCatched: boolean = false;

  @Input()
  pokemonId: number;

  constructor(
    private readonly trainerService: TrainerService,
    private readonly catchService: CatchService
  ) { }

  ngOnInit(): void {
    this.isCatched = this.trainerService.inCatched(this.pokemonId);
  }

  onCatchClick(): void {
    this.loading = true;
    this.catchService.addToCatched(this.pokemonId)
      .subscribe({
        next: (trainer: Trainer) => {
          this.loading = false;
          this.isCatched = this.trainerService.inCatched(this.pokemonId);
          console.log("Catched or not " + this.isCatched)
          console.log("Pokemon id is " + this.pokemonId)
        },
        error: (error: HttpErrorResponse) => {
          console.log("ERROR", error.message);
        }
      })
  }
}