import { Component, Input, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';

@Component({
  selector: 'app-pokemon-list-item',
  templateUrl: './pokemon-list-item.component.html'
})
export class PokemonListItemComponent implements OnInit {

  private _url: string = "";
  @Input() pokemon?: Pokemon;
  ngOnInit(): void {
  }

  getPokemonUrl(): string | undefined {
    if (this.pokemon?.url !== undefined) {
      const arr = this.pokemon?.url.split('/')
      this._url = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${arr[6]}.png`
    }
    return this._url;
  }
}