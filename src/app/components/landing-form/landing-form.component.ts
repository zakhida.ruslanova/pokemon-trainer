import { Component, EventEmitter, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Trainer } from 'src/app/models/trainer.model';
import { LandingService } from 'src/app/services/landing.service';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-landing-form',
  templateUrl: './landing-form.component.html',
})
export class LandingFormComponent {

  @Output() landing: EventEmitter<void> = new EventEmitter();

  constructor(

    private readonly landingService: LandingService,
    private readonly trainerService: TrainerService,

  ) { }

  public landingSubmit(landingForm: NgForm): void {

    const { username } = landingForm.value;

    this.landingService.landing(username)
      .subscribe({
        next: (trainer: Trainer) => {
          this.trainerService.trainer = trainer;
          this.landing.emit();
        },
        error: () => {
        }
      })
  }
}
