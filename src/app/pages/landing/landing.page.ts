import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.page.html'
})
export class LandingPage {

  constructor(private readonly router: Router) { }

  ngOnInit(): void {
  }

  handleLanding(): void {
    this.router.navigateByUrl("/pokemons")
  }
}