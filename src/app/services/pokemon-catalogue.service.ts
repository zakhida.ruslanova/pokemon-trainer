import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize, map } from 'rxjs';
import { environment } from 'src/environments/environment';
import { StorageKeys } from '../enums/storage-keys.enum';
import { Pokemon, PokemonResponse } from '../models/pokemon.model';
import { StorageUtil } from '../utils/storage.util';

const { apiPokemons } = environment;

@Injectable({
  providedIn: 'root'
})
export class PokemonCatalogueService {

  private _pokemons: Pokemon[] = [];
  private _error: string = "";
  private _loading: boolean = false;

  get pokemons(): Pokemon[] {
    return this._pokemons;
  }

  get error(): string {
    return this._error;
  }

  get loading(): boolean {
    return this._loading;
  }

  set pokemons(pokemons: Pokemon[]) {
    StorageUtil.storageSave<Pokemon[]>(StorageKeys.Pokemon, pokemons);
    this._pokemons = pokemons;
  }
  constructor(private readonly http: HttpClient) { }

  public findAllPokemons(): void {


    if (this._pokemons.length > 0 || this.loading) {
      return;
    }
    this._loading = true;

    this.http.get<PokemonResponse>(apiPokemons)
      .pipe(
        map((pokemonResponse: PokemonResponse) => {
          return pokemonResponse.results
        }),
        finalize(() => {
          this._loading = false;
        })
      )
      //exampel "url":"https://pokeapi.co/api/v2/pokemon/1/" take number "1" with slice
      .subscribe({
        next: (pokemons: Pokemon[]) => {
          console.log(pokemons);
          const pokemonArray = pokemons.map(pokemon => ({
            ...pokemon,
            id: Number(pokemon.url.split("/").slice(-2, -1))
          }))
          this.pokemons = pokemonArray;
        },
        error: (error: HttpErrorResponse) => {
          this._error = error.message;
        }
      })
  }
  public pokemonById(id: number): Pokemon | undefined {
    return this._pokemons.find((pokemon: Pokemon) => pokemon.id === id)
  }
}