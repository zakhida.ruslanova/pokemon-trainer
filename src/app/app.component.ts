import { Component, OnInit } from '@angular/core';
import { PokemonCatalogueService } from './services/pokemon-catalogue.service';
import { TrainerService } from './services/trainer.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {

  constructor(
    private readonly trainerService: TrainerService,
    private readonly pokemonService: PokemonCatalogueService
  ) {

  }
  ngOnInit(): void {
    //this.pokemonService.findAllPokemons();
    if(this.trainerService.trainer) {
      this.pokemonService.findAllPokemons();
    }
  }
}