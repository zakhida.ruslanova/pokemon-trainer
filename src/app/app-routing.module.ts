import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "./guards/auth.guard";
import { LandingPage } from "./pages/landing/landing.page";
import { PokemonCataloguePage } from "./pages/pokemon-catalogue/pokemon-catalogue.page";
import { TrainerProfilePage } from "./pages/trainer-profile/trainer-profile.page";

const routes: Routes = [{
    path: "",
    pathMatch: "full",
    redirectTo: "/landing"
},
{
    path: "landing",
    component: LandingPage
},
{
    path: "pokemons",
    component: PokemonCataloguePage,
    canActivate: [AuthGuard]
},
{
    path: "trainer",
    component: TrainerProfilePage,
    canActivate: [AuthGuard]
}
]

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {
}