## Assigment 3 -Create a Pokémon Trainer using Angular-

Build a Pokémon Trainer web app using the Angular Framework. 
-requirement-
• Angular framework
• Angular Router to navigate between pages
• Store the username and collected Pokémon in the Trainer API.
• Use Angular Services to manage the state of your application


## Deacription

This app has 3 sections to show the functions to login(landing) app or create new login name, display Pokémon Catalogue including add (catch) Pokémon -buttonand and display logined Pokémon trainer´s page to show catched pokemon including remove Pokémon -buttonand.

```
1) Landing Page
2) Trainer Page
3) Pokémon Catalogue Pagee
```

## Landing Page/Login Page

-  The trainer (user) must be able to enter their name and Save the Trainer name (username) to the Trainer API
-  When you login with the saved name, the app must be redirected to the main page, the Pokémon Catalogue page. 
-  The trainer may NOT be able to see the Pokémon Catalogue without have a Trainer name stored in sessionStorage.

***
Use a Guard service to achieve this functionality.
***
- If trainername exists in sessionStorage, you may automatically redirect from the landing page to the Pokémon Catalogue page.

***
Session storage can ONLY store strings. To stringify the data using JSON.stringify when 
storing objects.
***

## Trainer Page

- A Trainer(user) may only view this page if there is a Trainer name that exists in localStorage. 
- Redirect a user back to the Landing page if there is no Trainer name stored in localStorage. 
***
Use a Guard service to achieve this functionality.
***
- The Trainer page should list the Pokémon with name and image that the trainer has collected.
- The Trainer must be able to remove a Pokémon from their collection list in the Trainer page.

## Pokémon Catalogue Pagee

- The Catalogue page may NOT be displayed if there is no Trainer name stored in localStorage. 

***
Use a Guard service to achieve this functionality.
***

- The Catalogue page must list the Pokémon name and image (avatar).

***
Image: Example for Pokémon avatar with id of 1:
https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png
***

- Catch-Button: This should catch a Pokémon and add to the trainer´s collection list and it must update the Trainer API with the collected Pokémon.
- Poke ball-icon turn to check-icom  when the Pokémon has been collected.


***
***
# PokemonTrainer

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 15.1.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
